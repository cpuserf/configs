-- Neovim config
-- By Sean Long
-- tested on nvim v0.9.5

--
-- Bootstrap lazy.nvim
--
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  local lazyrepo = "https://github.com/folke/lazy.nvim.git"
  local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
  if vim.v.shell_error ~= 0 then
    vim.api.nvim_echo({
      { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
      { out, "WarningMsg" },
      { "\nPress any key to exit..." },
    }, true, {})
    vim.fn.getchar()
    os.exit(1)
  end
end
vim.opt.rtp:prepend(lazypath)

--
-- Make sure to setup `mapleader` and `maplocalleader` before
-- loading lazy.nvim so that mappings are correct.
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

--
-- general settings
--
vim.opt.number = true
vim.opt.ff = 'unix'
vim.opt.gfn = 'GoMono Nerd Font Mono:h11'
vim.opt.termguicolors = true

--
-- Setup lazy.nvim
--
require('lazy').setup({
	'mhinz/vim-startify',
	'nvim-tree/nvim-tree.lua',
	'RRethy/nvim-base16',
	{
		'nvim-lualine/lualine.nvim',
		dependencies = { 'nvim-tree/nvim-web-devicons' }
	},
	{
		'nvim-telescope/telescope.nvim',
		branch = '0.1.x',
		dependencies = { 'nvim-lua/plenary.nvim' }
	},
	{
		'VonHeikemen/lsp-zero.nvim',
		branch = 'v2.x',
		dependencies = {
			-- LSP Support
			'neovim/nvim-lspconfig',				-- Required
			'williamboman/mason.nvim',				-- Optional
			'williamboman/mason-lspconfig.nvim',	-- Optional

			-- Autocompletion
			'hrsh7th/nvim-cmp',		-- Required
			'hrsh7th/cmp-nvim-lsp', -- Required
			'L3MON4D3/LuaSnip',		-- Required
		}
	},
})

--
-- general settings after plug-ins loaded
--
vim.cmd('colorscheme base16-helios')
vim.cmd('highlight ColorColumn ctermbg=5')
vim.cmd('highlight ColorColumn guibg=DarkMagenta')
vim.cmd('highlight LineNr guibg=DimGrey')

--
-- plug-in specific settings
--

-- startify : Fancy start screen
-- ANSI FIGlet Font "ANSI Shadow"
-- from: http://www.patorjk.com/software/taag/#p=display&f=ANSI%20Shadow&t=NVIM
vim.g.startify_custom_header = {
	'    ███╗   ██╗██╗   ██╗██╗███╗   ███╗',
	'    ████╗  ██║██║   ██║██║████╗ ████║',
	'    ██╔██╗ ██║██║   ██║██║██╔████╔██║',
	'    ██║╚██╗██║╚██╗ ██╔╝██║██║╚██╔╝██║',
	'    ██║ ╚████║ ╚████╔╝ ██║██║ ╚═╝ ██║',
	'    ╚═╝  ╚═══╝  ╚═══╝  ╚═╝╚═╝     ╚═╝',
}
-- Manage sessions with:
-- :SLoad    load a session
-- :SSave    save a session
-- :SDelete  delete a session
-- :SClose   close current session

-- mason : Portable package manager for neovim
require('mason').setup()

-- lsp-zero : simple starting point to setup LSP
local lsp = require('lsp-zero').preset({})
lsp.on_attach(function(client, bufnr)
	-- see :help lsp-zero-keybindings
	-- to learn the available actions
	lsp.default_keymaps({ buffer = bufnr })
end)
-- (Optional) Configure lua language server for neovim
require('lspconfig').lua_ls.setup(lsp.nvim_lua_ls())
lsp.setup()

-- lualine : fast statusline for neovim
require('lualine').setup {
	options = {
		icons_enabled = true,
		theme = 'base16'
	}
}

-- nvim-tree : file explorer
require('nvim-tree').setup()

-- telescope : fuzzy finder for neovim
require('telescope').setup()

--
-- autocmds
--

-- disables number lines on terminal buffers
vim.api.nvim_create_autocmd('TermOpen', {
	pattern = '*',
	command = 'setlocal nonumber norelativenumber'
})

-- show a vertical line at column x while in insert mode
local virtlinegrp = vim.api.nvim_create_augroup('virtline', { clear = true })
vim.api.nvim_create_autocmd('InsertEnter', {
	pattern = '*',
	group = virtlinegrp,
	command = 'setlocal colorcolumn=120'
})
vim.api.nvim_create_autocmd('InsertLeave', {
	pattern = '*',
	group = virtlinegrp,
	command = 'setlocal colorcolumn=0'
})

--
-- custom commands
--

-- convenient command to see the difference between the current buffer and the
-- file it was loaded from, AKA the changes you made.
vim.cmd([[
 command! DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis | wincmd p | diffthis
]])

--
-- custom key bindings
--

-- clear any previous search highlighting
vim.keymap.set('n', '<leader><space>', ':noh<cr>', { noremap = true })

-- map <Leader>p or and <Leader>y to copy & paste to system clipboard
vim.keymap.set('v', '<leader>y', '"+y', {})
vim.keymap.set('v', '<leader>d', '"+d', {})
vim.keymap.set('v', '<leader>p', '"+p', {})
vim.keymap.set('v', '<leader>P', '"+P', {})
vim.keymap.set('n', '<leader>p', '"+p', {})
vim.keymap.set('n', '<leader>P', '"+P', {})

-- map keys to easily move between windows without using arrow keys
vim.keymap.set('n', '<C-h>', ':wincmd h<CR>', { silent = true })
vim.keymap.set('n', '<C-j>', ':wincmd j<CR>', { silent = true })
vim.keymap.set('n', '<C-k>', ':wincmd k<CR>', { silent = true })
vim.keymap.set('n', '<C-l>', ':wincmd l<CR>', { silent = true })

-- remap F1 to esc for times when miss hitting esc, can use :h for help
vim.keymap.set('i', '<F1>', '<ESC>', { noremap = true })
vim.keymap.set('n', '<F1>', '<ESC>', { noremap = true })
vim.keymap.set('v', '<F1>', '<ESC>', { noremap = true })

-- remap F6 to insert the current date (in local time zone)
vim.keymap.set('i', '<F6>', '<ESC>:r !date --iso-8601<CR>A', { noremap = true })
vim.keymap.set('n', '<F6>', ':r !date --iso-8601<CR>A', { noremap = true })
vim.keymap.set('n', '<F9>', ':NvimTreeToggle<CR>', { noremap = true })

-- bindings for telescope
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>b', builtin.buffers, {})
vim.keymap.set('n', '<leader>f', builtin.find_files, {})
vim.keymap.set('n', '<leader>h', builtin.help_tags, {})
vim.keymap.set('n', '<leader>c', builtin.live_grep, {})

-- make it easy to open the vimrc for editing & sourcing
vim.keymap.set('n', '<Leader>ev', ':vsplit $MYVIMRC<CR>', { noremap = true })
vim.keymap.set('n', '<Leader>sv', ':source $MYVIMRC<CR>', { noremap = true })
