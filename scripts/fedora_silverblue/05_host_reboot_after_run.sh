#!/bin/sh

# assuming running in a root shell (sudo -s)

printf "%% enabling sshd\n"

printf "~~ start sshd on bool\n"
systemctl enable sshd.service
printf "~~ start sshd now\n"
systemctl start sshd.service

printf "%% enabling flathub\n"

printf "~~ adding the flathub.org remote repo\n"
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

printf "%% rpm-ostree\n"

printf "~~ layer packages\n"
rpm-ostree install akmods distrobox neofetch ctags ripgrep fzf htop virt-manager neovim gnome-tweak-tool zsh net-tools intel-opencl mesa-libOpenCL intel-gpu-tools google-go-fonts google-go-mono-fonts google-go-smallcaps-fonts wl-clipboard python3-pip libtidy

printf "~~ remove un-needed packages from base OS\n"
rpm-ostree override remove firefox firefox-langpacks gnome-tour

printf "~~ fix the keys on apple laptops/keyboards\n"
rpm-ostree kargs --append=hid_apple.swap_opt_cmd=1 --append=hid_apple.fnmode=2

printf "~~ upgrade the system\n"
rpm-ostree upgrade

printf "!! you should reboot the system now\n"
