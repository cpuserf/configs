#!/bin/sh

USER=$USER

printf "%% add '$USER' to libvirt group\n"
grep -E '^libvirt:' /usr/lib/group | sudo tee -a /etc/group > /dev/null
sudo usermod -aG libvirt $USER

printf "%% install flatpaks\n"
sudo flatpak install -y flathub \
    com.github.tchx84.Flatseal \
    io.podman_desktop.PodmanDesktop \
    com.mattjakeman.ExtensionManager \
    org.gnome.Epiphany \
    org.mozilla.firefox \
    org.chromium.Chromium \
    org.libreoffice.LibreOffice \
    org.zim_wiki.Zim \
    org.qownnotes.QOwnNotes \
    com.github.alainm23.planner \
    com.nextcloud.desktopclient.nextcloud \
    org.gnome.gitlab.somas.Apostrophe \
    com.github.tenderowl.frog \
    com.github.subhadeepjasu.pebbles \
    com.github.johnfactotum.Foliate \
    ca.desrt.dconf-editor \
    nl.hjdskes.gcolor3 \
    com.github.wwmm.easyeffects \
    org.gnome.Lollypop \
    org.gnome.Rhythmbox3 \
    com.rafaelmardojai.Blanket \
    com.spotify.Client \
    com.github.louis77.tuner \
    org.kde.krita \
    org.gimp.GIMP \
    org.inkscape.Inkscape \
    org.darktable.Darktable \
    org.kde.digikam \
    org.flameshot.Flameshot \
    org.kde.kdenlive \
    com.makemkv.MakeMKV \
    com.valvesoftware.Steam \
    org.gnome.meld \
    com.gitlab.newsflash \
    org.gnome.World.PikaBackup

printf "%% uninstall unwanted flatpaks\n"
sudo flatpak uninstall -y org.gnome.Extensions

printf "%% fix stupid settings in GNOME\n"

printf "~~ make sure minimize and maximize are shown in the title bar of windows\n"
gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'
printf "~~ turn off the stupid hot corners\n"
gsettings set org.gnome.desktop.interface enable-hot-corners false
printf "~~ scale the screen size by scaling font, make bigger for my older eyes\n"
gsettings set org.gnome.desktop.interface text-scaling-factor '1.25'
printf "~~ show 12h time\n"
gsettings set org.gnome.desktop.interface clock-format '12h'
gsettings set org.gtk.settings.file-chooser clock-format '12h'
printf "~~ turn off 'Natural Scrolling'\n"
gsettings set org.gnome.desktop.peripherals.mouse natural-scroll false
gsettings set org.gnome.desktop.peripherals.touchpad natural-scroll false
printf "~~ set up a vertical gradiant background, primary is on top, secondary on bottom\n"
gsettings set org.gnome.desktop.background picture-uri none
gsettings set org.gnome.desktop.background picture-uri-dark none
gsettings set org.gnome.desktop.background picture-options none
gsettings set org.gnome.desktop.background primary-color '#364860'
gsettings set org.gnome.desktop.background secondary-color '#5EA400'
gsettings set org.gnome.desktop.background color-shading-type 'vertical'
printf "~~ when scroll bars are needed always show them\n"
gsettings set org.gnome.desktop.interface overlay-scrolling false
printf "~~ don't attach modal dialogs to the Window (like a Mac OS sheet), it is stupid\n"
gsettings set org.gnome.mutter attach-modal-dialogs false
printf "~~ put nautilus into list view mode\n"
gsettings set org.gnome.nautilus.preferences default-folder-viewer 'list-view'

printf "%% install the following extensions in the Extension Manager app launching now...\n"
printf "AppIndicator and KStatusNotifierItem Support\n"
printf "Tactile\n"
printf "Vitals\n"
printf "Impatience\n"
printf "No Overview at start-up\n"
flatpak run com.mattjakeman.ExtensionManager 2>/dev/null
