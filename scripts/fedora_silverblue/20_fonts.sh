#!/bin/sh

printf "%% installing Go Mono Nerd Fonts for user\n"
wget -O gomono_nerd.zip https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/Go-Mono.zip
unzip -d gomono_nerd gomono_nerd.zip
mkdir -p $HOME/.local/share/fonts
cp gomono_nerd/*.ttf $HOME/.local/share/fonts/
rm -rf gomono_nerd
rm -rf gomono_nerd.zip
fc-cache -f
