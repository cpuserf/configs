#!/bin/sh

OS_RELEASE_PATH=/etc/os-release

# find distro info
DIST_NAME_PRETTY=$(awk 'BEGIN {FS = "="} $1 == "PRETTY_NAME" {gsub("\"",""); print $2}' $OS_RELEASE_PATH)
DIST_NAME_TYPE=$(awk 'BEGIN {FS = "="} $1 == "ID" {gsub("\"",""); print $2}' $OS_RELEASE_PATH)
DIST_VARIANT=$(awk 'BEGIN {FS = "="} $1 == "VARIANT_ID" {print $2}' $OS_RELEASE_PATH)
DIST_IS_RHEL=$(awk 'BEGIN {FS = "="} $1 == "ID_LIKE" && $2 ~ /rhel/ {print 1}' $OS_RELEASE_PATH)
DIST_FULL_VERSION=$(awk 'BEGIN {FS = "="} $1 == "VERSION_ID" {gsub("\"",""); print $2}' $OS_RELEASE_PATH)
DIST_MAJOR_VERSION=$(awk 'BEGIN {FS = "="} $1 == "VERSION_ID" {split($2, subfield, "."); gsub("\"","",subfield[1]); printf("%d", subfield[1])}' $OS_RELEASE_PATH)
DIST_MINOR_VERSION=$(awk 'BEGIN {FS = "="} $1 == "VERSION_ID" {split($2, subfield, "."); gsub("\"","",subfield[2]); printf("%d", subfield[2])}' $OS_RELEASE_PATH)
DIST_SUPPORT_END_DATE=$(awk 'BEGIN {FS = "="} $1 == "SUPPORT_END" {print $2}' $OS_RELEASE_PATH)

# other system info
DIST_KERNEL=$(uname -r)
DIST_GLIBC_VERSION=$(ldd --version | head -n 1 | awk '{print $NF}')

# add defaults if needed
if [ ! "$DIST_VARIANT" ]; then
	DIST_VARIANT="unknown"
fi
if [ "$DIST_IS_RHEL" = "1" ]; then
	DIST_IS_RHEL="true"
	DIST_RHEL_MAJ_VERSION=$DIST_MAJOR_VERSION
	DIST_RHEL_MIN_VERSION=$DIST_MINOR_VERSION
else
	DIST_IS_RHEL="false"
	DIST_RHEL_MAJ_VERSION="n/a"
	DIST_RHEL_MIN_VERSION="n/a"
fi
if [ ! "$DIST_SUPPORT_END_DATE" ]; then
	DIST_SUPPORT_END_DATE="unknown"
fi

# tool versions
DIST_GCC_VERSION=$(gcc --version 2>/dev/null)
if [ "$?" = 0 ]; then
	DIST_GCC_VERSION=$(gcc --version | head -n 1 | awk '{gsub(")",""); print $NF}')
else
	DIST_GCC_VERSION="n/a"
fi

DIST_CMAKE_VERSION=$(cmake --version 2>&1>/dev/null)
if [ "$?" = 0 ]; then
	DIST_CMAKE_VERSION=$(cmake --version | head -n 1 | awk '{print $NF}')
else
	DIST_CMAKE_VERSION="n/a"
fi

DIST_AWK_VERSION=$(awk --version 2>/dev/null)
if [ "$?" = 0 ]; then
	DIST_AWK_VERSION=$(awk --version | head -n 1)
else
	DIST_AWK_VERSION=$(awk -W version 2>/dev/null | head -n 1)
fi

DIST_PYTHON_VERSION=$(python --version 2>/dev/null)
if [ "$?" = 0 ]; then
	DIST_PYTHON_VERSION=$(python --version | awk '{print $NF}')
else
	DIST_PYTHON_VERSION="n/a"
fi

DIST_PYTHON2_VERSION=$(python2 --version 2>/dev/null)
if [ "$?" = 0 ]; then
	# python2 puts the version out on stderr instead of stdout, :headslap:
	DIST_PYTHON2_VERSION=$(python2 --version 2>&1 | awk '{print $NF}')
else
	DIST_PYTHON2_VERSION="n/a"
fi

DIST_PYTHON3_VERSION=$(python3 --version 2>/dev/null)
if [ "$?" = 0 ]; then
	DIST_PYTHON3_VERSION=$(python3 --version | awk '{print $NF}')
else
	DIST_PYTHON3_VERSION="n/a"
fi

printf "name=%s\n" "$DIST_NAME_PRETTY"
printf "type=%s\n" "$DIST_NAME_TYPE"
printf "variant=%s\n" "$DIST_VARIANT"
printf "version=%s\n" "$DIST_FULL_VERSION"
printf "rhel=%s\n" "$DIST_IS_RHEL"
printf "rhel-maj-version=%s\n" "$DIST_RHEL_MAJ_VERSION"
printf "rhel-min-version=%s\n" "$DIST_RHEL_MIN_VERSION"
printf "support-end-date=%s\n" "$DIST_SUPPORT_END_DATE"
printf "kernel=%s\n" "$DIST_KERNEL"
printf "glibc=%s\n" "$DIST_GLIBC_VERSION"
printf "gcc=%s\n" "$DIST_GCC_VERSION"
printf "cmake=%s\n" "$DIST_CMAKE_VERSION"
printf "awk=%s\n" "$DIST_AWK_VERSION"
printf "python=%s\n" "$DIST_PYTHON_VERSION"
printf "python2=%s\n" "$DIST_PYTHON2_VERSION"
printf "python3=%s\n" "$DIST_PYTHON3_VERSION"
