#!/bin/sh
CPATH="$HOME/code/gitlab/configs/home"

# setup links to basic config files in $HOME
LIST="editorconfig gitconfig pycodestyle pydocstyle tmux.conf Xdefaults xprofile zshrc"
for i in $LIST; do
    ln -sf $CPATH/_$i $HOME/.$i
done

# setup links to basic config files in $HOME/.config
CFG_DIR_PATH="$CPATH/_config"
CFG_LIST="fontconfig rofi rsg"
for i in $CFG_LIST; do
    ln -sf $CFG_DIR_PATH/$i $HOME/.config/$i
done

# nvim
NVIM_INIT_FILE="$CPATH/_config/nvim/init.lua"
NVIM_CONFIG_DIR="$HOME/.config/nvim"
NVIM_CONFIG_FILE="$NVIM_CONFIG_DIR/init.lua"
mkdir -p $NVIM_CONFIG_DIR
ln -sf $NVIM_INIT_FILE $NVIM_CONFIG_FILE

# needed by nvim
python3 -m pip install --user --upgrade msgpack pynvim
