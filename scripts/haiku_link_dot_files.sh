#!/bin/sh

CPATH="$HOME/code/gitlab/configs/home"

SETTINGS_DIR="$HOME/config/settings"

mkdir -p "$SETTINGS_DIR/git"
mkdir -p "$SETTINGS_DIR/vim"
mkdir -p "$SETTINGS_DIR/zsh"

ln -sf "$CPATH/_gitconfig" "$SETTINGS_DIR/git/config"
ln -sf "$CPATH/_zshrc" "$SETTINGS_DIR/zsh/zshrc"
ln -sf "$CPATH/_tmux.conf" "$SETTINGS_DIR/tmux.conf"
ln -sf "$CPATH/_editorconfig" "$HOME/.editorconfig"
ln -sf "$CPATH/_pycodestyle" "$HOME/.pycodestyle"
ln -sf "$CPATH/_pydocstyle" "$HOME/.pydocstyle"

ln -sf "$CPATH/_vim/vimrc" "$SETTINGS_DIR/vim/vimrc"
ln -sf "$CPATH/_config/rsg" "$HOME/.rsg"

