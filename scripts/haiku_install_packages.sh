#!/bin/sh

# commandline tools
pkgman install ctags tmux vim

# applications
pkgman install quicklaunch

# get the Go fonts
git clone --depth 1 https://go.googlesource.com/image
cd image/font/gofont/ttfs
mimeset -F *.ttf
cp *.ttf ~/config/non-packaged/data/fonts/
cd -
rm -rf image
